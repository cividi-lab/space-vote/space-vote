# [1.2.0](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.1.0...v1.2.0) (2022-12-13)


### Bug Fixes

* beta and prod deploy based on tags ([a70a467](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/a70a467d49f2a975909830ca10df42402ca962b1))
* **ci:** auto-release branches ([af714f2](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/af714f24361f47eab71fc0df5b95a5593d1b8217))
* **ci:** ci triggers for tags ([109a906](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/109a9065d77f6208b8fe18ee2f6ec5fa23bf88ef))
* **ci:** fixed env url for stage ([28b20ed](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/28b20edb55b2ae1be0d559fbf5ab8015d1bb29b1))
* **ci:** tag references ([34ca637](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/34ca637a1d159529eedc9ea2e60ed34628ccf5cd))
* migrate to vercel ([a4a8697](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/a4a86971daf88ca9889dfb6b2705ff0cc1a87c48))
* migrate to vercel ([9383166](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/93831663635025e21ef336deecca09e323e54633))


### Features

* add vue-fathom package ([3b4ddfc](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/3b4ddfc8d3c8fc64a926115ce64705c1469888cb))
* vercel deployment ([eadb247](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/eadb247ec85a2749a6a9fab850aca6be6bc438ce))
* version footer ([e450030](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/e450030d21f920cd4260cf1f81437f1c2338ee2d))

# [1.1.0](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.0.0...v1.1.0) (2022-12-13)


### Features

* implement initial home, project, mint and result views ([a2fb187](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/a2fb18736e0d9c4eb11daaf2dbd31abb206eeb65))

# [1.1.0-beta.1](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.0.0...v1.1.0-beta.1) (2022-12-05)


### Bug Fixes

* beta and prod deploy based on tags ([a70a467](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/a70a467d49f2a975909830ca10df42402ca962b1))
* **ci:** auto-release branches ([af714f2](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/af714f24361f47eab71fc0df5b95a5593d1b8217))
* **ci:** ci triggers for tags ([109a906](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/109a9065d77f6208b8fe18ee2f6ec5fa23bf88ef))
* **ci:** fixed env url for stage ([28b20ed](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/28b20edb55b2ae1be0d559fbf5ab8015d1bb29b1))
* **ci:** tag references ([34ca637](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/34ca637a1d159529eedc9ea2e60ed34628ccf5cd))
* migrate to vercel ([a4a8697](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/a4a86971daf88ca9889dfb6b2705ff0cc1a87c48))
* migrate to vercel ([9383166](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/93831663635025e21ef336deecca09e323e54633))


### Features

* add vue-fathom package ([3b4ddfc](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/3b4ddfc8d3c8fc64a926115ce64705c1469888cb))
* vercel deployment ([eadb247](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/eadb247ec85a2749a6a9fab850aca6be6bc438ce))
* version footer ([e450030](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/e450030d21f920cd4260cf1f81437f1c2338ee2d))

# [1.0.0-beta.10](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.0.0-beta.9...v1.0.0-beta.10) (2022-12-05)


### Features

* vercel deployment ([eadb247](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/eadb247ec85a2749a6a9fab850aca6be6bc438ce))

# [1.0.0-beta.9](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.0.0-beta.8...v1.0.0-beta.9) (2022-12-05)


### Bug Fixes

* migrate to vercel ([a4a8697](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/a4a86971daf88ca9889dfb6b2705ff0cc1a87c48))

# [1.0.0-beta.8](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.0.0-beta.7...v1.0.0-beta.8) (2022-12-05)


 1.0.0 (2022-10-18)


### Bug Fixes

* refactor page publication ([120f39a](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/120f39afa6988a8ed966cbdf4ebc8c39621c4181))


### Features

* placeholder landing page ([f03d7e2](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/f03d7e2db2dbb676a501b78e66573cea1f929838))


### Bug Fixes

* migrate to vercel ([9383166](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/93831663635025e21ef336deecca09e323e54633))

# [1.0.0-beta.7](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.0.0-beta.6...v1.0.0-beta.7) (2022-10-18)


### Features

* add vue-fathom package ([3b4ddfc](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/3b4ddfc8d3c8fc64a926115ce64705c1469888cb))

# [1.0.0-beta.6](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.0.0-beta.5...v1.0.0-beta.6) (2022-10-17)


### Bug Fixes

* **ci:** fixed env url for stage ([28b20ed](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/28b20edb55b2ae1be0d559fbf5ab8015d1bb29b1))

# [1.0.0-beta.5](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.0.0-beta.4...v1.0.0-beta.5) (2022-10-17)


### Features

* version footer ([e450030](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/e450030d21f920cd4260cf1f81437f1c2338ee2d))

# [1.0.0-beta.4](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.0.0-beta.3...v1.0.0-beta.4) (2022-10-17)


### Bug Fixes

* **ci:** tag references ([34ca637](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/34ca637a1d159529eedc9ea2e60ed34628ccf5cd))

# [1.0.0-beta.3](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.0.0-beta.2...v1.0.0-beta.3) (2022-10-17)


### Bug Fixes

* **ci:** ci triggers for tags ([109a906](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/109a9065d77f6208b8fe18ee2f6ec5fa23bf88ef))

# [1.0.0-beta.2](https://gitlab.com/cividi-lab/space-vote/space-vote/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2022-10-17)


### Bug Fixes

* beta and prod deploy based on tags ([a70a467](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/a70a467d49f2a975909830ca10df42402ca962b1))

# 1.0.0-beta.1 (2022-10-17)


### Bug Fixes

* **ci:** auto-release branches ([af714f2](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/af714f24361f47eab71fc0df5b95a5593d1b8217))
* refactor page publication ([120f39a](https://gitlab.com/cividi-lab/space-vote/space-vote/commit/120f39afa6988a8ed966cbdf4ebc8c39621c4181))
