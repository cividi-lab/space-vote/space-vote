import type { StorybookConfig } from "@storybook/core-common";

const config: StorybookConfig = {
  stories: ["../src/**/*.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    // "storybook-facelift",
    "@storybook/addon-a11y",
    "storybook-addon-measure-viewport",
    "@storybook/addon-links",
    "storybook-addon-pseudo-states",
  ],
  framework: {
    name: "@storybook/vue3-vite",
    options: {},
  },
  core: {},
  features: {
    storyStoreV7: true,
  },
  staticDirs: ["../public"],
  env: (config) => ({
    ...config,
    VITE_VERSION: "v1.0.0",
    VITE_GIT_PROVIDER: "gitlab",
    VITE_GIT_PROVIDER_URL: "gitlab.com",
    VITE_GIT_REPO: "cividi/space-vote/space-vote",
  }),
};

module.exports = config;
