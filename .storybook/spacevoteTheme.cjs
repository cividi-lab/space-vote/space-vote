import { create } from "@storybook/theming";

export default create({
  base: "dark",
  brandTitle: "SpaceVote",
  brandUrl: "https://vote.cividi.space",
  brandImage: "/cividi-dark.svg",
  brandTarget: "_blank",

  // Colors
  colorPrimary: "hotpink",
  colorSecondary: "deepskyblue",

  // Typography
  fontBase: '"Open Sans", sans-serif',
  fontCode: "monospace",
});
