import { addons } from "@storybook/addons";
import spacevoteTheme from "./spacevoteTheme.cjs";

addons.setConfig({
  theme: spacevoteTheme,
});
