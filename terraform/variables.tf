variable "vercel_team_id" {
  type        = string
  description = "Vercel Team ID"
}

variable "repo_url" {
  type        = string
  description = "Repo URL"
}

variable "project_name" {
  type        = string
  description = "Project name"
}

variable "ref_slug" {
  type        = string
  description = "Ref Slug"
}

variable "zone" {
  type        = string
  description = "DNS zone to deploy to"
}

variable "name" {
  type        = string
  description = "Name for the deployment"
}

variable "fathom_id" {
  type        = string
  description = "Fathom ID"
  default     = ""
}
variable "fathom_cdn" {
  type        = string
  description = "Fathom CDN"
  default     = ""
}

variable "network_id" {
  type        = string
  description = "Ethereum network id"
}

variable "network_key" {
  type        = string
  sensitive   = true
  description = "Ethereum network key"
}

variable "admin_mnemonic" {
  type        = string
  sensitive   = true
  description = "Mnemonic for wallet"
}

variable "space_token_address" {
  type        = string
  description = "Ethereum address for space token"
}

variable "global_project_address" {
  type        = string
  description = "Ethereum address for global project"
}

variable "local_project_address" {
  type        = string
  description = "Ethereum address for local project"
}
