terraform {
  required_version = ">= 1.0"
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "3.20.0"
    }
    vercel = {
      source  = "vercel/vercel"
      version = "0.11.2"
    }
  }
}

data "cloudflare_zone" "zone" {
  name = var.zone
}

locals {
  is_prod           = var.ref_slug == "main" ? true : false
  is_stage          = var.ref_slug == "beta" ? true : false
  zone              = var.zone
  zone_id           = data.cloudflare_zone.zone.id
  subdomain         = var.name
  app_domain_prefix = local.is_stage || local.is_prod ? "" : "review-${var.ref_slug}."
}

resource "cloudflare_record" "fathom_custom_domain" {
  count   = var.fathom_cdn == "" || local.is_prod ? 0 : 1
  zone_id = local.zone_id
  name    = "${var.name}-cdn"
  value   = var.fathom_cdn
  type    = "CNAME"
}

resource "vercel_project" "project" {
  count = local.is_prod ? 0 : 1

  team_id   = var.vercel_team_id #
  name      = local.is_stage ? var.project_name : "${var.project_name}-${var.ref_slug}"
  framework = "vite"

  build_command              = "vue-tsc --noEmit && vite build"
  install_command            = "yarn install"
  serverless_function_region = "fra1"

  git_repository = {
    production_branch = "main"
    type              = "gitlab"
    repo              = var.repo_url
  }

  environment = [
    {
      key    = "VITE_FATHOM_ID"
      value  = var.fathom_id
      target = ["production", "preview", "development"]
    },
    {
      key    = "VITE_GIT_PROVIDER"
      value  = "gitlab"
      target = ["production", "preview", "development"]
    },
    {
      key    = "VITE_GIT_PROVIDER_URL"
      value  = "gitlab.com"
      target = ["production", "preview", "development"]
    },
    {
      key    = "VITE_GIT_REPO"
      value  = var.repo_url
      target = ["production", "preview", "development"]
    },

    {
      key    = "PROXY_NETWORK_ID"
      value  = var.network_id
      target = ["production", "preview", "development"]
    },

    {
      key    = "PROXY_NETWORK_KEY"
      value  = var.network_key
      target = ["production", "preview", "development"]
    },

    {
      key    = "PROXY_ADMIN_MNEMONIC"
      value  = var.admin_mnemonic
      target = ["production", "preview", "development"]
    },

    {
      key    = "PROXY_SPACE_TOKEN_ADDRESS"
      value  = var.space_token_address
      target = ["production", "preview", "development"]
    },

    {
      key    = "PROXY_GLOBAL_PROJECT_ADDRESS"
      value  = var.global_project_address
      target = ["production", "preview", "development"]
    },

    {
      key    = "PROXY_LOCAL_PROJECT_ADDRESS"
      value  = var.local_project_address
      target = ["production", "preview", "development"]
    },
  ]
}

resource "cloudflare_record" "project_main_domain" {
  count = local.is_prod ? 0 : 1

  zone_id = local.zone_id
  name    = "${local.app_domain_prefix}${var.name}"
  value   = "cname.vercel-dns.com"
  type    = "CNAME"
}

resource "vercel_project_domain" "main_domain" {
  count = local.is_prod ? 0 : 1

  project_id = vercel_project.project[0].id
  domain     = cloudflare_record.project_main_domain[0].hostname
  team_id    = var.vercel_team_id
}

resource "cloudflare_record" "project_stage_domain" {
  count = local.is_prod ? 0 : 1

  zone_id = local.zone_id
  name    = "${local.app_domain_prefix}stage.${var.name}"
  value   = "cname.vercel-dns.com"
  type    = "CNAME"
}

resource "vercel_project_domain" "stage_domain" {
  count = local.is_prod ? 0 : 1

  project_id = vercel_project.project[0].id
  domain     = cloudflare_record.project_stage_domain[0].hostname
  team_id    = var.vercel_team_id
  git_branch = "beta"
}
