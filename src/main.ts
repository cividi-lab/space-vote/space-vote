import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";
//import "./style.css";
import "./index.css";

import App from "./App.vue";
import Home from "./views/Home.vue";
import Project from "./views/Project.vue";
import Mint from "./views/Mint.vue";
import Result from "./views/Result.vue";

import * as Fathom from "fathom-client";

//kept routes here for simplicity
const routes = [
  { path: "/", component: Home },
  { path: "/project", component: Project },
  { path: "/mint", component: Mint },
  { path: "/result", component: Result },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

if (import.meta.env.VITE_FATHOM_ID) {
  Fathom.load(import.meta.env.VITE_FATHOM_ID, {
    url:
      import.meta.env.VITE_FATHOM_URL || "https://cdn.usefathom.com/script.js",
    honorDNT: true,
    excludedDomains: ["localhost", "www.local"],
  });
}

createApp(App).use(router).mount("#app");
