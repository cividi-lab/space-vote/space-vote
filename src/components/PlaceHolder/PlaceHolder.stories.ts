import PlaceHolder from "./PlaceHolder.vue";

import { Meta } from "@storybook/vue3";

export default {
  component: PlaceHolder,
  parameters: {
    chromatic: { viewports: [320, 1200] },
  },
} as Meta<typeof PlaceHolder>;

export const Default: StoryFn<typeof PlaceHolder> = () => ({
  components: { PlaceHolder },
  template: "<PlaceHolder />",
});
