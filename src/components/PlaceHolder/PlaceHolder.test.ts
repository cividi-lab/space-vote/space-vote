import { render } from "@testing-library/vue";
import { test } from "vitest";
import Placeholder from "./PlaceHolder.vue";

test("Placeholder.vue", () => {
  const { getByText } = render(Placeholder);

  getByText("SpaceVote");
});
