import VersionTag from "./VersionTag.vue";

import { Meta, Story } from "@storybook/vue3";

const defaultArgs = {
  provider: "gitlab",
  providerUrl: "gitlab.com",
  repo: "cividi/space-vote/space-vote",
};

export default {
  component: VersionTag,
  parameters: {
    chromatic: { viewports: [320, 1200] },
  },
} as Meta<typeof VersionTag>;

export const Prod: Story = {
  args: {
    version: "v1.0.0",
    ...defaultArgs,
  },
  render: (args) => ({
    components: { VersionTag },
    setup() {
      return { args };
    },
    template: '<VersionTag v-bind="args" />',
  }),
};

export const Beta: Story = {
  args: {
    version: "v1.0.0-beta.1",
    ...defaultArgs,
  },
  render: (args) => ({
    components: { VersionTag },
    setup() {
      return { args };
    },
    template: '<VersionTag v-bind="args" />',
  }),
};

export const PreviewNoMR: Story = {
  args: {
    version: "v1.0.0-beta.1",
    versionHash: "05336c4",
    ...defaultArgs,
  },
  render: (args) => ({
    components: { VersionTag },
    setup() {
      return { args };
    },
    template: '<VersionTag v-bind="args" />',
  }),
};

export const OpenMR: Story = {
  args: {
    version: "v1.0.0-beta.1",
    versionHash: "05336c4",
    mergeRequest: 11,
    mergeRequestBranch: "1-version-footer",
    ...defaultArgs,
  },
  render: (args) => ({
    components: { VersionTag },
    setup() {
      return { args };
    },
    template: '<VersionTag v-bind="args" />',
  }),
};

export const Missing: Story = {
  args: {
    ...defaultArgs,
  },
  render: (args) => ({
    components: { VersionTag },
    setup() {
      return { args };
    },
    template: '<VersionTag v-bind="args" />',
  }),
};
