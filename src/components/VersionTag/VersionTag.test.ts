import { render } from "@testing-library/vue";
import { it } from "vitest";
import VersionTag from "./VersionTag.vue";

it("renders gitlab with prod version set", () => {
  const { getByRole } = render(VersionTag, {
    props: { version: "v1.0.0" },
  });

  getByRole("link", { name: "v1.0.0" });
});

it("renders gitlab with stage version set", () => {
  const { getByRole } = render(VersionTag, {
    props: { version: "v1.0.0-beta.1" },
  });

  getByRole("link", { name: "v1.0.0-beta.1" });
});

it("renders gitlab with testing and no mr set", () => {
  const { getByRole } = render(VersionTag, {
    props: {
      version: "v1.0.0-beta.1",
      versionHash: "05336c4",
    },
  });

  getByRole("link", { name: "v1.0.0-beta.1+05336c4" });
});

it("renders gitlab with open mr set", () => {
  const { getByRole } = render(VersionTag, {
    props: {
      version: "v1.0.0-beta.1",
      versionHash: "05336c4",
      mergeRequest: 11,
      mergeRequestBranch: "1-feat-version-footer",
    },
  });

  getByRole("link", { name: "v1.0.0-beta.1+05336c4 [1-feat-version-footer]" });
});

it("renders gitlab with open mr and no version set", () => {
  const { getByRole } = render(VersionTag, {
    props: {
      versionHash: "05336c4",
      mergeRequest: 11,
      mergeRequestBranch: "1-feat-version-footer",
    },
  });

  getByRole("link", { name: "05336c4 [1-feat-version-footer]" });
});

it("renders gitlab with missing version", () => {
  const { getByRole } = render(VersionTag, {
    props: {
      repo: "cividi-lab/space-vote/space-vote",
    },
  });

  getByRole("link", {
    name: "cividi-lab/space-vote/space-vote [no version set]",
  });
});

it("renders gitlab with missing data", () => {
  const { getByRole } = render(VersionTag, {});

  getByRole("link", {
    name: "[no version set]",
  });
});
