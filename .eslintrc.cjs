/*global module, require*/

module.exports = {
  root: true,
  plugins: ["@typescript-eslint", "prettier"],
  parserOptions: {
    parser: {
      ts: require.resolve("@typescript-eslint/parser"),
      tsx: require.resolve("@typescript-eslint/parser")
    },
    extraFileExtensions: [".vue"]
  },
  extends: ["eslint:recommended", "plugin:@typescript-eslint/recommended", "plugin:vue/vue3-recommended", "prettier", "plugin:prettier/recommended", "plugin:storybook/recommended"],
  rules: {
    "prettier/prettier": "error"
  }
};