/// <reference types="vitest" />
import { defineConfig } from "vite";
import * as path from "path";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src"),
    },
  },
  base: "./",
  test: {
    environment: "jsdom",
    reporters: ["default", "junit"],
    outputFile: "junit.xml",
    coverage: {
      reporter: ["cobertura", "text", "text-summary"],
    },
  },
});
