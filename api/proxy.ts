import type { VercelRequest, VercelResponse } from "@vercel/node";
import json from "../src/assets/data.json";
import { point, polygon, Feature, booleanWithin, coordAll } from "@turf/turf";
import {
  providers,
  Wallet,
  utils,
  BigNumber,
  Contract,
  constants,
} from "ethers";

function projectList(res: VercelResponse) {
  return res.json({
    ...json,
  });
}

function projectForLocation(res: VercelResponse, point: Feature) {
  // Return project matching given location

  console.log(
    `Fetching project for ${point.geometry.type} ${coordAll(point)}...`
  );
  const firstMatch: number = json.features.findIndex((element) => {
    const ply = polygon(element.geometry.coordinates);
    return booleanWithin(point, ply);
  });

  if (firstMatch != -1) {
    console.log(`Found a match at index ${firstMatch}`);
    return res.json({
      request: {
        point,
      },
      status: "success",
      result: json.features[firstMatch],
    });
  }

  return res.json({
    request: {
      point,
    },
    status: "no active project found at location",
    result: null,
  });
}

async function triggerContract(
  res: VercelResponse,
  uuid: string,
  lng: number,
  lat: number,
  projectNr: number,
  voteOption: number
) {
  // Start the minting process
  console.log(`Minting token for ${lat} ${lng}...`);

  const networkId = process.env.PROXY_NETWORK_ID || null;
  const networkKey = process.env.PROXY_NETWORK_KEY || null;
  const adminMnemonic = process.env.PROXY_ADMIN_MNEMONIC || null;

  const spaceTokenAddress = process.env.PROXY_SPACE_TOKEN_ADDRESS || null;
  const globalProjectAddress = process.env.PROXY_GLOBAL_PROJECT_ADDRESS || null;
  const localProjectAddress = process.env.PROXY_LOCAL_PROJECT_ADDRESS || null;

  const spaceTokenAbi = [
    // Some details about the token
    "function name() view returns (string)",
    "function symbol() view returns (string)",

    // Get the account balance
    "function balanceOf(address owner) view returns (uint256)",

    // Get the owner of a token
    "function ownerOf(uint256 tokenId) view returns (address)",

    // Mint a new SpaceToken
    "function mint(uint256 spaceTokenID, uint8 v_ben, bytes32 r_ben, bytes32 s_ben, uint8 v_ver, bytes32 r_ver, bytes32 s_ver)",

    // An event triggered whenever anyone transfers to someone else
    "event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)",

    "function assembleSpaceTokenID(uint32 blockTime, uint32 lat1000000, uint32 long1000000, address minter) pure returns (uint256)",

    "function getBlockTime(uint256 spaceTokenID) pure returns (uint32)",

    "function getLat1000000(uint256 spaceTokenID) pure returns (uint32)",

    "function getLong1000000(uint256 spaceTokenID) pure returns (uint32)",

    "function getMinter(uint256 spaceTokenID) pure returns (address)",
  ];
  const spaceProjectAbi = [
    "function _setProjectParameters(uint256 _projectLocation, uint256 _projectLocationDelta, uint _maxOption)",

    "function vote(uint256 spaceTokenID, uint8 v_ben, bytes32 r_ben, bytes32 s_ben, uint8 v_ver, bytes32 r_ver, bytes32 s_ver, uint option)",

    "function getNumberOfVotes(uint option) view returns (uint)",

    "function getVotingToken(uint option, uint index) view returns (uint)",

    "function assembleSpaceTokenID(uint32 blockTime, uint32 lat1000000, uint32 long1000000, address minter) pure returns (uint256)",

    "function getBlockTime(uint256 spaceTokenID) pure returns (uint32)",

    "function getLat1000000(uint256 spaceTokenID) pure returns (uint32)",

    "function getLong1000000(uint256 spaceTokenID) pure returns (uint32)",

    "function getMinter(uint256 spaceTokenID) pure returns (address)",
  ];

  if (
    networkId &&
    networkKey &&
    adminMnemonic &&
    spaceTokenAddress &&
    globalProjectAddress &&
    localProjectAddress &&
    uuid &&
    uuid != ""
  ) {
    // Vote for selected project "projectNr" with vote "voteOption" (mints a new SpaceToken)

    // Get the default provider for the given network
    // const provider = ethers.getDefaultProvider("http://localhost:8545");
    // const provider = ethers.getDefaultProvider("goerli");

    const provider = new providers.EtherscanProvider(networkId, networkKey);
    console.log(provider);

    // Use the provider to look up the current block number
    const currentBlock = await provider.getBlockNumber();
    console.log(currentBlock);

    // Get the user's main wallet which will be used to create the one-time keys
    const userWallet = new Wallet(utils.id(uuid));

    // Get the admin's wallet which will be used to pay the user's gas fees
    const mnemonic = adminMnemonic.replaceAll("_", " ");
    console.log(mnemonic);
    const adminWallet = Wallet.fromMnemonic(mnemonic);
    const payerWallet = adminWallet.connect(provider);
    console.log("Payer pool address: " + payerWallet.address);

    // Use the user's main wallet to sign the current block number,
    // and use the signature as a private key to generate a (one-time) wallet
    // TODO: add some salt?
    const signature = await userWallet.signMessage(currentBlock.toString());
    const privateKey = utils.id(signature); // make sure key length is 256 bit
    console.log(privateKey);
    const wallet = await new Wallet(privateKey, provider);
    console.log("One-time address: " + wallet.address);

    const pos = {
      lat,
      lng,
    };
    console.log(pos);

    // The Contract object, connected to the payer wallet that will pay for the transaction
    // Two example projects implemented so far (one global, one local)
    let spaceProject;
    if (projectNr == 0) {
      spaceProject = await new Contract(
        globalProjectAddress,
        spaceProjectAbi,
        payerWallet
      );
    } else {
      spaceProject = await new Contract(
        localProjectAddress,
        spaceProjectAbi,
        payerWallet
      );
    }
    const spaceTokenContract = await new Contract(
      spaceTokenAddress,
      spaceTokenAbi,
      payerWallet
    );

    // construct tokenID from time, location and minter address
    const time_int = BigNumber.from(currentBlock);
    const lat_int = BigNumber.from(Math.floor(1000000 * pos.lat) + 500000000);
    const lng_int = BigNumber.from(Math.floor(1000000 * pos.lng) + 500000000);
    const tokenID = await spaceProject.assembleSpaceTokenID(
      time_int,
      lat_int,
      lng_int,
      wallet.address
    );

    console.log(
      "Minting new SpaceToken ID=" +
        tokenID.toString() +
        " and voting option=" +
        voteOption.toString() +
        " for project=" +
        projectNr.toString() +
        ". This might take a while (blockchain transaction!)"
    );

    // TODO: each the beneficiary (one-time wallet) and a verifier must sign a EIP-712 digest of
    // beneficiary address and tokenID. The two signatures are passed to and required by mint()
    const hashedID = utils.keccak256(
      utils.solidityPack(["uint256"], [tokenID])
    );
    const rawSignature = await wallet.signMessage(utils.arrayify(hashedID));
    const bensig = utils.splitSignature(rawSignature);
    console.log(bensig);
    const v_ben = bensig.v;
    const r_ben = bensig.r;
    const s_ben = bensig.s;
    const v_ver = 0;
    const r_ver = constants.HashZero;
    const s_ver = constants.HashZero;

    // Check gas
    try {
      const gasEstimate = await spaceProject.estimateGas.vote(
        tokenID,
        v_ben,
        r_ben,
        s_ben,
        v_ver,
        r_ver,
        s_ver,
        voteOption
      );
      const feeData = await provider.getFeeData();
      let gasCost: BigNumber = BigNumber.from("100");
      if (feeData && feeData.maxFeePerGas) {
        gasCost = gasEstimate.mul(feeData.maxFeePerGas);
      }
      if (gasCost.gt(utils.parseEther("0.02"))) {
        const costs = utils.formatEther(gasCost);
        console.log(
          "Gas prices are crazy right now, transaction would cost " +
            costs +
            " ETH, aborting..."
        );
        return;
      }
      const tx = await spaceProject.vote(
        tokenID,
        v_ben,
        r_ben,
        s_ben,
        v_ver,
        r_ver,
        s_ver,
        voteOption,
        {
          maxFeePerGas: feeData.maxFeePerGas,
          maxPriorityFeePerGas: feeData.maxPriorityFeePerGas,
        }
      );
      console.log(tx);

      // Wait for the transaction to be mined...
      await tx.wait();
      console.log(tx);

      // Get the owner of the new token
      const owner = await spaceTokenContract.ownerOf(tokenID);
      console.log("Successfully minted token ID = " + tokenID.toString());
      console.log(
        "SpaceToken ID=" +
          tokenID.toString() +
          " successfully minted to owner: " +
          owner
      );
      return res.json({
        request: {
          lat,
          lng,
          projectNr,
          voteOption,
        },
        status: "minted token",
        result: {
          token: tokenID.toString(),
        },
      });
    } catch (error) {
      console.log(error);
      const err = String(error);
      const errPos = err.indexOf("execution reverted");
      let errorMsg = "Mint failed: Unknown error";
      if (errPos > 0) {
        errorMsg = "Mint failed: " + err.substring(errPos, errPos + 70);
      }
      console.log(errorMsg);
      return res.json({
        request: {
          lat,
          lng,
          projectNr,
          voteOption,
        },
        status: errorMsg,
        result: null,
      });
    }
  }
}

async function getProjectVotes(res: VercelResponse, projectAddress: string) {
  const networkId = process.env.PROXY_NETWORK_ID || null;
  const networkKey = process.env.PROXY_NETWORK_KEY || null;

  const spaceProjectAbi = [
    "function _setProjectParameters(uint256 _projectLocation, uint256 _projectLocationDelta, uint _maxOption)",

    "function vote(uint256 spaceTokenID, uint8 v_ben, bytes32 r_ben, bytes32 s_ben, uint8 v_ver, bytes32 r_ver, bytes32 s_ver, uint option)",

    "function getNumberOfVotes(uint option) view returns (uint)",

    "function getVotingToken(uint option, uint index) view returns (uint)",

    "function assembleSpaceTokenID(uint32 blockTime, uint32 lat1000000, uint32 long1000000, address minter) pure returns (uint256)",

    "function getBlockTime(uint256 spaceTokenID) pure returns (uint32)",

    "function getLat1000000(uint256 spaceTokenID) pure returns (uint32)",

    "function getLong1000000(uint256 spaceTokenID) pure returns (uint32)",

    "function getMinter(uint256 spaceTokenID) pure returns (address)",
  ];

  if (networkId && networkKey) {
    // Get the provider
    const provider = new providers.EtherscanProvider(networkId, networkKey);

    const projectData = json.features.filter(
      (project) => project.properties.projectContractAddress === projectAddress
    );

    const projectContract = await new Contract(
      projectAddress,
      spaceProjectAbi,
      provider
    );

    const projectVotes = [];
    let votesCounted = 0;

    const bar = new Promise<void>((resolve) => {
      projectData[0].properties.projectVoteOptions.forEach(
        (voteOption, index, array) => {
          projectContract
            .getNumberOfVotes(voteOption.id)
            .then((voteCount: number) => {
              projectContract
                .getVotingToken(voteOption.id, voteCount - 1)
                .then((lastVote: undefined) => {
                  console.log("Reached the inside loop");
                  projectVotes[index] = {
                    id: voteOption.id,
                    text: voteOption.text,
                    count: voteCount.toString(),
                    lastVote: lastVote.toString(),
                  };
                  votesCounted += 1;
                  if (votesCounted === array.length) resolve();
                });
            });
        }
      );
    });

    bar.then(() => {
      console.log("All done!");
      return res.json({
        request: {
          projectData,
        },
        status: "success",
        result: {
          projectVotes,
        },
      });
    });
  }
}

export default function (req: VercelRequest, res: VercelResponse) {
  const {
    lat = "",
    lng = "",
    projectNr,
    voteOption,
    uuid = "",
    projectAddress = "",
    action = "projectList",
  } = req.query;

  switch (action) {
    case "projectList":
      projectList(res);
      break;
    case "projectForLocation":
      projectForLocation(res, point([+lng, +lat]));
      break;
    case "triggerContract":
      triggerContract(res, `${uuid}`, +lng, +lat, +projectNr, +voteOption);
      break;
    case "getProjectVotes":
      getProjectVotes(res, `${projectAddress}`);
      break;
  }
}
