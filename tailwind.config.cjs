/** @type {import('tailwindcss').Config} */
module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.ts'
    ],
  theme: {
    extend: {},
  },
  plugins: [],
}
